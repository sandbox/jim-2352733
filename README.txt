-- SUMMARY --

The Database Stream Wrapper module was originally developed to fix an issue
with a load balancing system (https://www.drupal.org/node/2352763).
It wasn't able to provide consistent files content across all the web
nodes - the shared storage got stuck randomly. This module provides
the consistency using a database. It stores all the files content in
a database and copies it to a file once the module starts working with the file.
It means that the database has "reference" content which is being loaded at
the beginning and put back to the database at the end.

-- REQUIREMENTS --

None. Note that all the files content is stored in database, so you need plenty
of DB server space to store them all.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --

Use db:// schema wherever you want to have consistent DB storage.
For instance, Views Data Export directory:
variable_set('views_data_export_directory', 'db://views_plugin_display');
