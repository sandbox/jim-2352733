<?php

/**
 * @file
 * Drupal stream wrapper implementation for a Database File Storage.
 *
 * Implements DrupalStreamWrapperInterface to provide a Database File Storage stream
 * using the "db://" scheme.
 */

/**
 * The stream wrapper class.
 */
class DatabaseStreamWrapper extends DrupalPrivateStreamWrapper {
  /**
   * Put the content from a database to the file before working with it.
   *
   * {@inheritdoc}
   */
  public function stream_open($uri, $mode, $options, &$opened_path) {
    $this->uri = $uri;
    $path = $this->getLocalPath();
    $row = db_select('database_stream_wrapper_files', 'f')
      ->fields('f', array('data'))
      ->condition('f.name', $this->getUri())
      ->execute()
      ->fetchAssoc();

    if ($row) {
      file_put_contents($path, $row['data']);
    }

    return parent::stream_open($uri, $mode, $options, $opened_path);
  }

  /**
   * Put the content from the file to database after working with it.
   *
   * {@inheritdoc}
   */
  public function stream_close() {
    parent::stream_close();

    $data = file_get_contents($this->getLocalPath());
    $row = array(
      'name' => $this->getUri(),
      'data' => $data,
    );
    $primary_keys = array();
    $update = db_select('database_stream_wrapper_files', 'f')
      ->condition('f.name', $this->getUri())->countQuery()->execute()
      ->fetchField();
    if ($update) {
      $primary_keys = array('name');
    }
    drupal_write_record('database_stream_wrapper_files', $row, $primary_keys);
  }

  /**
   * {@inheritdoc}
   */
  public function unlink($uri) {
    db_delete('database_stream_wrapper_files')
      ->condition('name', $uri)
      ->execute();

    return parent::unlink($uri);
  }
}
